document.addEventListener("DOMContentLoaded", function () {
  let _cpf = generateCpf();
  let _curp = generateCurp();
  let _cnpj = generateCnpj();

  document.querySelector('#generatorButton').addEventListener('click', function () {
    document.getElementById('msgOk').style.display = 'none';
    _cpf = generateCpf();
    _curp = generateCurp();
    _cnpj = generateCnpj();
  });

  document.querySelector('#copy-cpf').addEventListener('click', function () {
    copyToClipboard(_cpf);
  });

  document.querySelector('#copy-curp').addEventListener('click', function () {
    copyToClipboard(_curp);
  });

  document.querySelector('#copy-cnpj').addEventListener('click', function () {
    copyToClipboard(_cnpj);
  });

  function copyToClipboard(value) {
    const copyText = document.createElement("input");
    document.getElementById('html').appendChild(copyText);
    copyText.setAttribute("id", "toCopy");
    copyText.value = value;
    copyText.select();
    copyText.setSelectionRange(0, 99999)
    document.execCommand("copy");
    copyText.remove();
    document.getElementById('msgOk').style.display = 'block';
  }

  /**
   * CPF
   */
  function randomizes(n) {
    let num = Math.round(Math.random() * n);
    return num;
  }

  function mod(dividend, divisor) {
    return Math.round(dividend - (Math.floor(dividend / divisor) * divisor));
  }

  function generateCpf() {
    // TRUE enable and FALSE disable the dots
    let withDots = false;
    let checkboxValue = document.getElementById("withDots");

    if (checkboxValue.checked == true) withDots = true

    let n = 9;
    let n1 = randomizes(n);
    let n2 = randomizes(n);
    let n3 = randomizes(n);
    let n4 = randomizes(n);
    let n5 = randomizes(n);
    let n6 = randomizes(n);
    let n7 = randomizes(n);
    let n8 = randomizes(n);
    let n9 = randomizes(n);

    let d1 = n9 * 2 + n8 * 3 + n7 * 4 + n6 * 5 + n5 * 6 + n4 * 7 + n3 * 8 + n2 * 9 + n1 * 10;
    d1 = 11 - (mod(d1, 11));

    if (d1 >= 10) d1 = 0;

    let d2 = d1 * 2 + n9 * 3 + n8 * 4 + n7 * 5 + n6 * 6 + n5 * 7 + n4 * 8 + n3 * 9 + n2 * 10 + n1 * 11;
    d2 = 11 - (mod(d2, 11));

    if (d2 >= 10) d2 = 0;

    retorno = '';

    if (withDots == true)
      cpf = '' + n1 + n2 + n3 + '.' + n4 + n5 + n6 + '.' + n7 + n8 + n9 + '-' + d1 + d2;
    else
      cpf = '' + n1 + n2 + n3 + n4 + n5 + n6 + n7 + n8 + n9 + d1 + d2;

    let cpfValido = validateCpf(cpf);

    if (cpfValido === true)
      document.getElementById("cpf").innerHTML = cpf;
    else
      document.getElementById("cpf").innerHTML = '';

    return cpf;
  }

  function validateCpf(cpf) {
    cpf = cpf.replace(/[^\d]+/g, '');

    if (cpf == '') return false;

    // Eliminate known invalid CPFs	
    if (cpf.length != 11 ||
      cpf == "00000000000" ||
      cpf == "11111111111" ||
      cpf == "22222222222" ||
      cpf == "33333333333" ||
      cpf == "44444444444" ||
      cpf == "55555555555" ||
      cpf == "66666666666" ||
      cpf == "77777777777" ||
      cpf == "88888888888" ||
      cpf == "99999999999")
      return false;

    // Validate 1st digit	
    add = 0;

    for (i = 0; i < 9; i++)
      add += parseInt(cpf.charAt(i)) * (10 - i);

    rev = 11 - (add % 11);

    if (rev == 10 || rev == 11) rev = 0;

    if (rev != parseInt(cpf.charAt(9))) return false;

    // Validate 2nd digit	
    add = 0;

    for (i = 0; i < 10; i++)
      add += parseInt(cpf.charAt(i)) * (11 - i);

    rev = 11 - (add % 11);

    if (rev == 10 || rev == 11) rev = 0;

    if (rev != parseInt(cpf.charAt(10))) return false;

    return true;
  }


  /**
   * CURP
   */
  function generateCurp() {
    let curp = '';

    while (validateCurp(curp) == false) {
      curp = new RandExp(/[A-Z]{4}\d{6}[HM][A-Z]{2}[B-DF-HJ-NP-TV-Z]{3}[A-Z0-9][0-9]/).gen();
    }

    document.getElementById("curp").innerHTML = curp;
    return curp;
  }

  // Validates whether the given value follows the CURP definition
  function validateCurp(curp) {
    let reg = "";

    if (curp.length == 18) {
      curp = curp.toUpperCase()
      let digit = checkCurp(curp);

      reg = /[A-Z]{4}\d{6}[HM][A-Z]{2}[B-DF-HJ-NP-TV-Z]{3}[A-Z0-9][0-9]/;

      if (curp.search(reg)) return false;

      if (!(parseInt(digit) == parseInt(curp.substring(17, 18)))) return false;

      return true;
    } else {
      return false;
    }
  }

  // Check the "check digit" using LUHN algorithm "tropicalized"
  function checkCurp(curp) {
    let segRoot = curp.substring(0, 17);
    let chrCaracter = "0123456789ABCDEFGHIJKLMNÑOPQRSTUVWXYZ";
    let intFactor = new Array(17);
    let lngSuma = 0.0;
    let lngdigit = 0.0;

    for (let i = 0; i < 17; i++) {
      for (let j = 0; j < 37; j++) {
        if (segRoot.substring(i, i + 1) == chrCaracter.substring(j, j + 1)) intFactor[i] = j;
      }
    }

    for (let k = 0; k < 17; k++) {
      lngSuma = lngSuma + ((intFactor[k]) * (18 - k));
    }

    lngdigit = (10 - (lngSuma % 10));

    if (lngdigit == 10) lngdigit = 0;

    return lngdigit;
  }

  function generateCnpj() {
    const withDots = document.getElementById("withDots").checked;

    var n = 9;
    var n1 = randomizes(n);
    var n2 = randomizes(n);
    var n3 = randomizes(n);
    var n4 = randomizes(n);
    var n5 = randomizes(n);
    var n6 = randomizes(n);
    var n7 = randomizes(n);
    var n8 = randomizes(n);
    var n9 = 0;
    var n10 = 0;
    var n11 = 0;
    var n12 = 1;
    var d1 = n12 * 2 + n11 * 3 + n10 * 4 + n9 * 5 + n8 * 6 + n7 * 7 + n6 * 8 + n5 * 9 + n4 * 2 + n3 * 3 + n2 * 4 + n1 * 5;
    d1 = 11 - (mod(d1, 11));
    if (d1 >= 10) d1 = 0;
    var d2 = d1 * 2 + n12 * 3 + n11 * 4 + n10 * 5 + n9 * 6 + n8 * 7 + n7 * 8 + n6 * 9 + n5 * 2 + n4 * 3 + n3 * 4 + n2 * 5 + n1 * 6;
    d2 = 11 - (mod(d2, 11));
    if (d2 >= 10) d2 = 0;

    let cnpj = '';
    if (withDots)
      cnpj = '' + n1 + n2 + '.' + n3 + n4 + n5 + '.' + n6 + n7 + n8 + '/' + n9 + n10 + n11 + n12 + '-' + d1 + d2;
    else
      cnpj = '' + n1 + n2 + n3 + n4 + n5 + n6 + n7 + n8 + n9 + n10 + n11 + n12 + d1 + d2;

    document.getElementById("cnpj").innerHTML = cnpj;

    return cnpj;
  }
});
